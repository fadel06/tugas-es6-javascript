// Template Literals
// ----------------------------------------------------------
// const planet = "earth" 
// const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' +       
// 			 'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +     
// 			 'incididunt ut labore et dolore magna aliqua. Ut enim' +     
// 			 ' ad minim veniam'   
// Driver Code 
// console.log(before) 

// Jawaban :
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet consetectur adipiscing elit ${planet} do euismod tempor incididunt ut lbore et dolore magna aliqua. Ut enim ad minim veniam `
console.log(before)