// Mengubah fungsi menjadi fungsi arrow
// -----------------------------------------
// const golden = function goldenFunction(){
//   console.log("this is golden!!")
// }
 
// golden()

// Jawaban : 
const golden = () => {
	console.log("this is golden!!")
}
golden()